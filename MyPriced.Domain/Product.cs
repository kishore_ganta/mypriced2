﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyPriced.Domain
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public decimal Price { get; set; }
    }
}
