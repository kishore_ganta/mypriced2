﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyPriced.Domain
{
    public class MyPricedDbContext:DbContext
    {
        public MyPricedDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

       public DbSet<Product> Products { get; set; }
    }
}
